<?php

function getPage($stmt, $pageNum, $rowsPerPage)
{
	$offset = ($pageNum - 1) * $rowsPerPage;
	$rows = array();
	$i = 0;
	while(($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC, SQLSRV_SCROLL_ABSOLUTE, $offset + $i)) && $i < $rowsPerPage)
	{
		array_push($rows, $row);
		$i++;
	}
	return $rows;
}

// Set the number of rows to be returned on a page.
$rowsPerPage = 10;

// Connect to the server.
$serverName = '.\sqlexpress';
$connOptions = array("UID" => "user_name", "PWD" => "password", "Database"=>"AdventureWorks");
$conn = sqlsrv_connect($serverName, $connOptions);
if (!$conn)
	die( print_r( sqlsrv_errors(), true));

// Define and execute the query.  
// Note that the query is executed with a "scrollable" cursor.
$sql = "SELECT Name, ProductNumber FROM Production.Product";

$stmt = sqlsrv_query($conn, $sql, array(), array( "Scrollable" => 'static' ));
if ( !$stmt )
	die( print_r( sqlsrv_errors(), true));

// Get the total number of rows returned by the query.
$rowsReturned = sqlsrv_num_rows($stmt);
if($rowsReturned === false)
    die( print_r( sqlsrv_errors(), true));
elseif($rowsReturned == 0)
{
    echo "No rows returned.";
	exit();
}
else
{     
    /* Calculate number of pages. */
    $numOfPages = ceil($rowsReturned/$rowsPerPage);
}

// Display the selected page of data.
echo "<table border='1px'>";
$pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
$page = getPage($stmt, $pageNum, $rowsPerPage);

foreach($page as $row)
	echo "<tr><td>$row[0]</td><td>$row[1]</td></tr>";

echo "</table><br />";

// Display Previous Page link if applicable.
if($pageNum > 1)
{
	$prevPageLink = "?pageNum=".($pageNum - 1);
	echo "<a href='$prevPageLink'>Previous Page</a>";
}

// Display Next Page link if applicable.
if($pageNum < $numOfPages)
{
	$nextPageLink = "?pageNum=".($pageNum + 1);
	echo "&nbsp;&nbsp;<a href='$nextPageLink'>Next Page</a>";
}

sqlsrv_close( $conn );
?>
