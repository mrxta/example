<?php
function getPage($stmt, $pageNum, $rowsPerPage)
{
	$offset = ($pageNum - 1) * $rowsPerPage;
	$rows = array();
	$i = 0;
	while(($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_ABS, $offset + $i)) && $i < $rowsPerPage)
	{
		array_push($rows, $row);
		$i++;
	}
	return $rows;
}

// Set the number of results to display on each page.
$rowsPerPage = 10;

try
{
	$conn = new PDO( "sqlsrv:server=.\sqlexpress ; Database=AdventureWorks", "user_name", "password");
	$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
}
catch(Exception $e)
{ 
	die( print_r( $e->getMessage() ) ); 
}

try
{
	// Define and execute the query.  
	// Note that the query is executed with a "scrollable" cursor.
	$sql = "SELECT Name, ProductNumber FROM Production.Product";

	$stmt = $conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
	$stmt->execute();
	
	$rowsReturned = $stmt->rowCount(); 
	if($rowsReturned === false)
		die( print_r( sqlsrv_errors(), true));
	elseif($rowsReturned == 0)
	{
		echo "No rows returned.";
		exit();
	}
	else
	{     
		// Display page links.
		$numOfPages = ceil($rowsReturned/$rowsPerPage);
		for($i = 1; $i<=$numOfPages; $i++)
    {
        $pageLink = "?pageNum=$i";
        print("<a href=$pageLink>$i</a>&nbsp;&nbsp;");
    }
    echo "<br/><br/>";
	}
	
	// Display the selected page of data.
	echo "<table border='1px'>";
	$pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
	$page = getPage($stmt, $pageNum, $rowsPerPage);

	foreach($page as $row)
	echo "<tr><td>$row[0]</td><td>$row[1]</td></tr>";

	echo "</table>";
}
catch(Exception $e)
{ 
	die( print_r( $e->getMessage() ) ); 
}
?>